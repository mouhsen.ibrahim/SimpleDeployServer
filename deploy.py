#!/usr/bin/env python

import os, sys, time, atexit
import BaseHTTPServer
import ConfigParser
import json
import argparse

from signal import SIGTERM

from git import Repo

class Daemon:
        """
        A generic daemon class.

        Usage: subclass the Daemon class and override the run() method
        """
        def __init__(self, pidfile, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
                self.stdin = stdin
                self.stdout = stdout
                self.stderr = stderr
                self.pidfile = pidfile

        def daemonize(self):
                """
                do the UNIX double-fork magic, see Stevens' "Advanced
                Programming in the UNIX Environment" for details (ISBN 0201563177)
                http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
                """
                try:
                        pid = os.fork()
                        if pid > 0:
                                # exit first parent
                                sys.exit(0)
                except OSError, e:
                        sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
                        sys.exit(1)

                # decouple from parent environment
                os.chdir("/")
                os.setsid()
                os.umask(0)

                # do second fork
                try:
                        pid = os.fork()
                        if pid > 0:
                                # exit from second parent
                                sys.exit(0)
                except OSError, e:
                        sys.stderr.write("fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
                        sys.exit(1)

                # redirect standard file descriptors
                sys.stdout.flush()
                sys.stderr.flush()
                si = file(self.stdin, 'r')
                so = file(self.stdout, 'a+', 0)
                se = file(self.stderr, 'a+', 0)
                os.dup2(si.fileno(), sys.stdin.fileno())
                os.dup2(so.fileno(), sys.stdout.fileno())
                os.dup2(se.fileno(), sys.stderr.fileno())

                # write pidfile
                atexit.register(self.delpid)
                pid = str(os.getpid())
                file(self.pidfile,'w+').write("%s\n" % pid)

        def delpid(self):
                os.remove(self.pidfile)

        def start(self):
                """
                Start the daemon
                """
                # Check for a pidfile to see if the daemon already runs
                try:
                        pf = file(self.pidfile,'r')
                        pid = int(pf.read().strip())
                        pf.close()
                except IOError:
                        pid = None

                if pid:
                        message = "pidfile %s already exist. Daemon already running?\n"
                        sys.stderr.write(message % self.pidfile)
                        sys.exit(1)

                # Start the daemon
                self.daemonize()
                self.run()

        def stop(self):
                """
                Stop the daemon
                """
                # Get the pid from the pidfile
                try:
                        pf = file(self.pidfile,'r')
                        pid = int(pf.read().strip())
                        pf.close()
                except IOError:
                        pid = None

                if not pid:
                        message = "pidfile %s does not exist. Daemon not running?\n"
                        sys.stderr.write(message % self.pidfile)
                        return # not an error in a restart

                # Try killing the daemon process
                try:
                        while 1:
                                os.kill(pid, SIGTERM)
                                time.sleep(0.1)
                except OSError, err:
                        err = str(err)
                        if err.find("No such process") > 0:
                                if os.path.exists(self.pidfile):
                                        os.remove(self.pidfile)
                        else:
                                print str(err)
                                sys.exit(1)

        def restart(self):
                """
                Restart the daemon
                """
                self.stop()
                self.start()

        def run(self):
                """
                You should override this method when you subclass Daemon. It will be called after the process has been
                daemonized by start() or restart().
                """


class NotProjectRepoError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class NotProjectBranchError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class ConfigDirError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class MissingSectionError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class WrongRequestError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class ProjectRepo(Repo):
    """
        This class represents a single local repo to work with
    """
    def __init__(self, git_dir):
        """
            raises NoSuchPathError if git_dir is not a directory
            raises InvalidGitRepositoryError if git_dir is invalid git repo
        """
        super(ProjectRepo, self).__init__(git_dir)
        self.git_dir = git_dir

class ProjectBranch(ProjectRepo):
    """
        This class represents a single git branch to work with
    """
    def __init__(self, git_dir, branch_name, work_tree):
        """
            raises OSError if work_tree does not exist and cannot be created
        """
        super(ProjectBranch, self).__init__(git_dir)

        self.branch_name = branch_name
        if not os.path.isdir(work_tree):
            # work tree does not exist try to create it
            os.mkdir(work_tree)
        self.work_tree = work_tree
    def update_work_tree(self):
        os.environ["GIT_DIR"] = self.git_dir
        os.environ["GIT_WORK_TREE"] = self.work_tree

        self.git.fetch("origin")
        self.git.reset("--hard", "origin/%s" % self.branch_name)



class Project(object):
    """
        This class represents a single gitlab Project
        to work with
    """
    def __init__(self, file_name, repo, branches):
        super(Project, self).__init__()
        self.file_name = file_name
        # an instance of ProjectRepo representing the local repo
        if type(repo) is not ProjectRepo:
            raise(NotProjectRepoError())
        self.repo = repo
        # a list of ProjectBranch instances
        for branche in branches:
            if(type(branche) is not ProjectBranch):
                raise(NotProjectBranchError)
        self.branches = branches

class SimpleRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_POST(self):
        # process post requests
        # return response without any delay so gitlab will not send another request
        self.send_response(200)
        message = "OK"
        self.send_header("Content-type", "text")
        self.send_header("Content-length", str(len(message)))
        self.end_headers()
        self.wfile.write(message)

        # processing a post request consists of stages
        # stage 1: check if the post request is from gitlab server
        # it should have X-Gitlab-Event header if the header does not
        # exist return
        # TODO: raise the WrongRequestError also if the request is coming from
        # untrusted server
        try:
            self.checkRequest()
        except WrongRequestError as e:
            # TODO: log events
            return
        # stage 2: get event type
        event_type = self.headers["X-Gitlab-Event"]
        # now only process push events
        if event_type == "Push Hook":
            # check if there is an options file for the remote repo and if the
            # branch is defined and has a work tree then update it.
            length = int(self.headers["Content-length"])
            data = self.rfile.read(length)
            dataJson = json.loads(data)
            name = dataJson["project"]["path_with_namespace"].replace("/", ".")
            project = self.server.get_project(name)
            branch_name = dataJson["ref"].split("/")[2]
            branch = self.server.get_branch(name, branch_name)
            if branch is not None:
                branch.update_work_tree()

    def checkRequest(self):
        try:
            self.headers["X-Gitlab-Event"]
        except KeyError as e:
            raise WrongRequestError()

class SimpleDeployServer(BaseHTTPServer.HTTPServer):
    """
        This class represents the main server
        It has a list of Project instances
    """
    def __init__(self, config_dir, options_file):
        # make sure config dir exists and options file exists
        if not os.path.isdir(config_dir):
            raise(ConfigDirError("config dir " + config_dir + " does not exist"))
        if not os.path.isfile(os.path.join(config_dir, options_file)):
            raise(ConfigDirError("options file " + options_file + " does not exist in config dir " + config_dir))
        config = ConfigParser.ConfigParser()
        config.readfp(open(os.path.join(config_dir, options_file)))
        # make sure server section exists
        if not config.has_section("server"):
            raise(MissingSectionError("config file " + os.path.join(config_dir, options_file) + " does not have server section"))
        # make sure host option exists
        host = config.get("server", "host")
        # make sure port option exists and can be converted to int and is greater than 1024
        port = config.get("server", "port")
        port = int(port)
        assert(port > 1024)
        self.config_dir = config_dir
        self.options_file = options_file
        self.projects = []
        self.read_config_files()
        BaseHTTPServer.HTTPServer.__init__(self, (host, port), SimpleRequestHandler)

    def read_config_files(self):
        files = [f for f in os.listdir(self.config_dir) if os.path.isfile(os.path.join(self.config_dir,f))]
        for file in files:
            project = self.parse_project(file)
            if project is not None:
                self.projects.append(project)

    def parse_project(self, file):
        file_path = os.path.join(self.config_dir, file)
        config = ConfigParser.ConfigParser()
        try:
            config.readfp(open(file_path))
            repo_path = config.get("repo", "git_dir")
            repo = ProjectRepo(repo_path)
            branches = []
            for section in config.sections():
                if (section != "repo"):
                    branch_name = section
                    work_tree = config.get(section, "work_tree")
                    branch = ProjectBranch(repo_path, branch_name, work_tree)
                    if branch is not None:
                        branches.append(branch)
            return Project(file, repo, branches)
        except Exception as e:
            return None

    def get_project(self, name):
        for project in self.projects:
            if project.file_name == name:
                return project
        return None
    def get_branch(self, name, branch_name):
        project = self.get_project(name)
        if project is None:
            return None
        for branch in project.branches:
            if branch.branch_name == branch_name:
                return branch
        return None


    def run_server(self):
        self.serve_forever()

class MyDaemon(Daemon):

    def run(self):
        HOME = os.environ["HOME"]
        server = SimpleDeployServer(HOME + "/.deploy", "options.ini")
        server.run_server()
    def status(self):
        try:
            pf = file(self.pidfile,'r')
        except IOError as e:
            return False
        pid = int(pf.read().strip())
        pf.close()
        try:
            os.kill(pid, 0)
        except OSError as e:
            return False
        else:
            return True

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", choices=["start","stop","restart", "status"])
    args = parser.parse_args()
    HOME = os.environ["HOME"]
    if args.d:
        STDOUT = HOME + "/.deploy/deploy.out"
        STDERR = HOME + "/.deploy/deploy.err"
        PID = HOME + "/.deploy/deploy.pid"
        myDaemon = MyDaemon(PID, "/dev/null", STDOUT, STDERR)
        if args.d == "start":
            myDaemon.start()
        elif args.d == "stop":
            myDaemon.stop()
        elif args.d == "restart":
            myDaemon.restart()
        else:
            if myDaemon.status():
                print("daemon is running")
            else:
                print("daemon is NOT running")
    else:
        server = SimpleDeployServer(HOME + "/.deploy", "options.ini")
        server.run_server()
