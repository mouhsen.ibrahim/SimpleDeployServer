import unittest
import ConfigParser
import requests
import threading
import json

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from deploy import ProjectRepo, Project, ProjectBranch, NotProjectRepoError, NotProjectBranchError, SimpleDeployServer, \
ConfigDirError, MissingSectionError
import os
from git import InvalidGitRepositoryError, NoSuchPathError

class MyThread(threading.Thread):
    def __init__(self, server):
        threading.Thread.__init__(self)
        self.server = server
    def run(self):
        self.server.run_server()

class TestDeploy(unittest.TestCase):

    def test_project_class(self):
        pwd = os.environ["PWD"]
        invalid_repo_path = pwd + "/git"
        non_existing_path = pwd + "/git/non"
        repo_path = pwd + "/git/repo"
        work_tree = pwd + "/work/test"
        work_tree_not_exists = pwd + "/work/non"
        work_tree_not_writable = pwd + "/work/not_writable/test"

        # test ProjectRepo class
        with self.assertRaises(InvalidGitRepositoryError):
            ProjectRepo(invalid_repo_path)
        with self.assertRaises(NoSuchPathError):
            ProjectRepo(non_existing_path)
        repo = ProjectRepo(repo_path)
        self.assertIsNotNone(repo)

        # test ProjectBranch class
        branch = ProjectBranch(repo_path, "master_branch", work_tree_not_exists)
        self.assertIsNotNone(branch)
        self.assertTrue(os.path.isdir(work_tree_not_exists))
        with self.assertRaises(OSError):
            ProjectBranch(repo_path, "master_branch", work_tree_not_writable)
        branch = ProjectBranch(repo_path, "master_branch", work_tree)
        self.assertIsNotNone(branch)

        # test Project class
        with self.assertRaises(NotProjectRepoError):
            Project("config_file", branch, [branch])
        with self.assertRaises(NotProjectBranchError):
            Project("config_file", repo, [repo])
        project = Project("config_file", repo, [branch])
        self.assertEqual(project.file_name, "config_file")
        self.assertEqual(project.repo.git_dir, repo_path)
        self.assertEqual(project.branches[0].branch_name, "master_branch")
        self.assertEqual(project.branches[0].work_tree, work_tree)

    def test_simple_deploy_server(self):
        pwd = os.environ["PWD"]
        config_dir_not_exists = os.path.join(pwd, "config_not")
        config_dir = os.path.join(pwd, "config")
        options_file = "options.ini"
        options_file_not_exists = "test.ini"
        options_file_invalid = "invalid_file.ini"
        options_no_server_section = "no_server.ini"
        options_no_port = "no_port.ini"
        options_no_host = "no_host.ini"
        options_invalid_port = "invalid_port.ini"
        options_small_port = "small_port.ini"
        options_empty_port = "empty_port.ini"
        options_empty_host = "empty_host.ini"
        with self.assertRaises(ConfigDirError):
            SimpleDeployServer(config_dir_not_exists, options_file)
        with self.assertRaises(ConfigDirError):
            SimpleDeployServer(config_dir, options_file_not_exists)
        with self.assertRaises(ConfigParser.ParsingError):
            SimpleDeployServer(config_dir, options_file_invalid)
        with self.assertRaises(MissingSectionError):
            SimpleDeployServer(config_dir, options_no_server_section)
        with self.assertRaises(ConfigParser.NoOptionError):
            SimpleDeployServer(config_dir, options_no_host)
        with self.assertRaises(ConfigParser.NoOptionError):
            SimpleDeployServer(config_dir, options_no_port)
        with self.assertRaises(ValueError):
            SimpleDeployServer(config_dir, options_invalid_port)
        with self.assertRaises(AssertionError):
            SimpleDeployServer(config_dir, options_small_port)
        with self.assertRaises(ValueError):
            SimpleDeployServer(config_dir, options_empty_port)
        server = SimpleDeployServer(config_dir, options_file)
        self.assertIsNotNone(server)

    def test_simple_request_handler(self):
        pwd = os.environ["PWD"]
        config_dir = os.path.join(pwd, "config")
        options_file = "options.ini"

        server = SimpleDeployServer(config_dir, options_file)
        thread = MyThread(server)
        thread.setDaemon(True)
        thread.start()

        content = {
            "object_kind": "push",
            "ref": "refs/heads/master",
            "project":{
                "path_with_namespace": "root/chat"
            }
        }
        content = json.dumps(content)
        headers = {
            "X-Gitlab-Event": "Push Hook",
            "Content-type": "application/json",
            "Content-length": str(len(content))
        }
        res = requests.post("http://localhost:8000", headers=headers, data=content)
        self.assertEqual(res.text, "OK")
